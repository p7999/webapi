from flask import Flask,request

app = Flask(__name__)

@app.route('/personal_user_info/<user_id>', methods = ['GET'])
def user(user_id):
    return {
        "name":"Avi",
        "email" : "avi@paygov.com",
        "city":"Rishon Lezion",
        "bank_account_number":"",
        "bank_branch_number":"",
        "id":"",
        "phone_number":"050-0000000"
    }



@app.route('/user_info/<user_id>', methods = ['GET'])
def user_info(user_id):
    return {
        "name":"Avi",
        "email" : "avi@paygov.com"
    }


@app.route('/payments/<user_id>', methods = ['GET'])
def user_payments(user_id):
    return {
        "police":[{
            "type": "report",
            "receiveDate":"2022-03-20T023:35:20.382",
            "paidDate": "",
            "isPaid": "false"
        }],
        "waterBill":[{
            "type": "bill",
            "receiveDate": "2022-03-20T023:35:20.382",
            "paidDate": "2022-03-21T023:35:20.382",
            "isPaid": "true"
        }]
    }



@app.route('/update_personal_info', methods = ['POST'])
def update_personal_info():
    request_data = request.json


@app.route('/login/<user_id>', methods = ['POST'])
def login(user_id):
    return {}

@app.route('/signup', methods = ['POST'])
def signup():
    request_data = request.json


@app.route('/company_signup', methods = ['POST'])
def company_signup():
    request_data = request.json
    if "name" not in request_data:
        return "Company name is required"
    if "baseApi" not in request_data:
        return "Base api organization is required . If you did not receive one during the poc process, please contact us"




@app.route("/")
def index():
    return "PayGov New World"

app.run()